#include "logindialog.h"
#include "ui_logindialog.h"

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    mUI(new Ui::LoginDialog)
{
    mUI->setupUi(this);
    QRegExp rxEMail("\\b[^|@]+@[^|@\\.]+\\.[A-Za-z]{2,4}\\b");
    QRegExp rxPass("\\b[^|]*\\b");
    mUI->leEmail->setValidator(new QRegExpValidator(rxEMail, this));
    mUI->lePassword->setValidator(new QRegExpValidator(rxPass, this));
}

LoginDialog::~LoginDialog()
{
    delete mUI;
}

void LoginDialog::on_buttonBox_accepted()
{
    auto email = mUI->leEmail->text();
    auto password = mUI->lePassword->text();
    emit login(email, password);
}
