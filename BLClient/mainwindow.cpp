#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow{parent},
    mUI{new Ui::MainWindow},
    mClient{nullptr}
{
    mUI->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete mUI;
}

void MainWindow::onLogin(const QString &email, const QString &password)
{
    if(nullptr != mClient)
        delete mClient;
    mClient = new Client(email, password, this);
    mClient->connectToServer();
    delete mLoginDialog;
}

void MainWindow::on_action_Sign_in_triggered()
{
    mLoginDialog = new LoginDialog(this);
    connect(mLoginDialog, SIGNAL(login(QString,QString)),
            this, SLOT(onLogin(QString,QString)));
    mLoginDialog->show();
}

void MainWindow::on_actionSign_out_triggered()
{
    if(nullptr != mClient) {
        mClient->disconnectFromServer();
    }
}
