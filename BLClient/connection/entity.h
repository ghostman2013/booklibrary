#ifndef ENTITY_H
#define ENTITY_H

#include <QtGlobal>
#include <QString>

class Entity
{
public:
    static const qint32 INVALID_ID = -1;
    Entity();
    qint32 id();
    void setId(qint32 id);

private:
    qint32 mID;
};

class Branch: public Entity {
public:
    Branch();
    void setName(const QString &name);
    void setParentId(qint32 parentId);
    const QString &name();
    qint32 parentId();
private:
    QString mName;
    qint32 mParentId;
};

class Book: public Entity {
public:
    Book();
    void setTitle(const QString &title);
    void setBranchId(qint32 branchId);
    const QString &title();
    qint32 branchId();
private:
    QString mTitle;
    qint32 mBranchId;
};

class BookDetails: public Book {
public:
    BookDetails();
};

#endif // ENTITY_H
