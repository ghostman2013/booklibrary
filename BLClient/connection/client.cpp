#include "client.h"

Client::Client(const QString &email, const QString &password, QObject *parent):
    QObject{parent},
    mBlockSize{0},
    mEmail{email},
    mPassword{password}
{
    mSocket = new QTcpSocket(this);
    connect(mSocket, SIGNAL(connected()), this, SLOT(onConnected()));
    connect(mSocket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    connect(mSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}

Client::~Client()
{
    delete mSocket;
}

bool Client::connectToServer()
{
    mSocket->connectToHost(QHostAddress::AnyIPv4, 9999);
    return true;
}

void Client::disconnectFromServer()
{
    mSocket->disconnectFromHost();
}

void Client::onConnected()
{
    qDebug() << "Connected!";
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    //Reserve 4 bytes
    out << (quint32)0 << mEmail << mPassword;
    //Back to the beginning
    out.device()->seek(0);
    //Write a block size
    out << (quint32)(block.size() - sizeof(quint32));
    mSocket->write(block);
}

void Client::onDisconnected()
{
    qDebug() << "Disconnected!";
}

void Client::onReadyRead()
{
    qDebug() << "Ready to read!";
    QDataStream in(mSocket);
    in.setVersion(QDataStream::Qt_4_0);

    for (;;) {

        if(!mBlockSize) {
            if (mSocket->bytesAvailable() < (quint32)sizeof(quint32)) break;
            in >> mBlockSize;
        }

        if(mSocket->bytesAvailable() < mBlockSize) break;

        QString tag;
        quint32 count;
        in >> tag >> count;

        qDebug() << "Package was recieved!";
        qDebug() << "Tag: " << tag << " Count: " << count;
        qDebug() << "Block size: " << mBlockSize;
        mBlockSize = 0;
    }
}

