#include "entity.h"

Entity::Entity():
    mID{INVALID_ID}
{}

qint32 Entity::id()
{
    return mID;
}

void Entity::setId(qint32 id)
{
    mID = id;
}


Branch::Branch():
    Entity{},
    mParentId{INVALID_ID}
{

}

void Branch::setName(const QString &name)
{
    mName = name;
}

void Branch::setParentId(qint32 parentId)
{
    mParentId = parentId;
}

const QString &Branch::name()
{
    return mName;
}

qint32 Branch::parentId()
{
    return mParentId;
}

Book::Book():
    Entity{},
    mBranchId{INVALID_ID}
{}

void Book::setTitle(const QString &title)
{
    mTitle = title;
}

void Book::setBranchId(qint32 branchId)
{
    mBranchId = branchId;
}

const QString &Book::title()
{
    return mTitle;
}

qint32 Book::branchId()
{
    return mBranchId;
}

BookDetails::BookDetails():
    Book{}
{}
