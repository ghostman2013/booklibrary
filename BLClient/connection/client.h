#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>
#include <QByteArray>
#include <QDataStream>
#include <QDebug>

class Client: public QObject
{
    Q_OBJECT
public:
    explicit Client(const QString &email,
                    const QString &password,
                    QObject *parent = 0);
    ~Client();
    bool connectToServer();
    void disconnectFromServer();
public slots:
    void onConnected();
    void onDisconnected();
    void onReadyRead();
private:
    QTcpSocket *mSocket;
    quint32 mBlockSize;
    QString mEmail;
    QString mPassword;
};

#endif // CLIENT_H
