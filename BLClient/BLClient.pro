#-------------------------------------------------
#
# Project created by QtCreator 2015-11-30T00:54:58
#
#-------------------------------------------------

QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QMAKE_CXXFLAGS += -std=c++11

TARGET = BLClient
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    connection/client.cpp \
    dialog/logindialog.cpp \
    connection/entity.cpp

HEADERS  += mainwindow.h \
    connection/client.h \
    dialog/logindialog.h \
    connection/entity.h

FORMS    += mainwindow.ui \
    dialog/logindialog.ui
