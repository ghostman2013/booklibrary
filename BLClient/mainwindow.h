#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "connection/client.h"
#include "dialog/logindialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onLogin(const QString &email, const QString &password);
    void on_action_Sign_in_triggered();
    void on_actionSign_out_triggered();

private:
    Ui::MainWindow *mUI;
    LoginDialog *mLoginDialog;
    Client *mClient;
};

#endif // MAINWINDOW_H
