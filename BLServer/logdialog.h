#ifndef LOGDIALOG_H
#define LOGDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <database/databasehandler.h>

namespace Ui {
class LogDialog;
}

class LogDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LogDialog(DatabaseHandler *db,
            QWidget *parent = 0);
    ~LogDialog();

private slots:
    void on_cbBranch_currentIndexChanged(int index);
    void on_cbClass_currentIndexChanged(int index);

    void on_buttonBox_accepted();

private:
    void updateClasses(qint32 tagId);
    void updateBooks(qint32 bookId);
    void check();
    Ui::LogDialog *ui;
    DatabaseHandler *mDB;
    std::list<User> mUsers;
    std::list<Tag> mTags;
    std::list<Book> mBooks;
    std::list<Item> mItems;
};

#endif // LOGDIALOG_H
