QT += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QMAKE_CXXFLAGS += -std=c++11

TARGET = BLServer

TEMPLATE = app

SOURCES += main.cpp \
    mainwindow.cpp \
    logindialog.cpp \
    database/table.cpp \
    database/databasehandler.cpp \
    database/utils.cpp \
    branchdialog.cpp \
    bookdialog.cpp \
    itemdialog.cpp \
    userdialog.cpp \
    logdialog.cpp \
    menu/contextmenu.cpp \
    server/server.cpp \
    server/client.cpp

HEADERS += \
    mainwindow.h \
    logindialog.h \
    database/table.h \
    database/databasehandler.h \
    database/utils.h \
    branchdialog.h \
    bookdialog.h \
    itemdialog.h \
    version.h \
    userdialog.h \
    logdialog.h \
    menu/contextmenu.h \
    utils/template.h \
    server/server.h \
    server/client.h

FORMS += \
    mainwindow.ui \
    logindialog.ui \
    branchdialog.ui \
    bookdialog.ui \
    itemdialog.ui \
    userdialog.ui \
    logdialog.ui

