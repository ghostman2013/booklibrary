#include "logindialog.h"
#include "ui_logindialog.h"

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog{parent},
    ui{new Ui::LoginDialog}
{
    ui->setupUi(this);
    ui->leDatabase->setText(DatabaseHandler::DEFAULT_DB_NAME);
    ui->leHost->setText(DatabaseHandler::DEFAULT_DB_HOST);
    ui->sbPort->setValue(DatabaseHandler::DEFAULT_DB_PORT);
    //Tab focus order
    QWidget::setTabOrder(ui->leLogin, ui->lePassword);
    QWidget::setTabOrder(ui->lePassword, ui->leDatabase);
    QWidget::setTabOrder(ui->leDatabase, ui->leHost);
    QWidget::setTabOrder(ui->leHost, ui->sbPort);
    QWidget::setTabOrder(ui->sbPort, ui->buttonBox);
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

QString LoginDialog::database() const
{
    return ui->leDatabase->text();
}

QString LoginDialog::host() const
{
    return ui->leHost->text();
}

int LoginDialog::port() const
{
    return ui->sbPort->value();
}

void LoginDialog::on_buttonBox_accepted()
{
    auto login = ui->leLogin->text();
    auto password = ui->lePassword->text();
    emit accept(login, password);
}
