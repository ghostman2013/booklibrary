#include "bookdialog.h"
#include "ui_bookdialog.h"

BookDialog::BookDialog(DatabaseHandler &db, Book &book, QWidget *parent) :
    QDialog{parent},
    mUI{new Ui::BookDialog},
    mDB{db},
    mBook{book}
{
    mUI->setupUi(this);

    if(mDB.tags(mTags))
        for(auto i = mTags.begin(), e = mTags.end(); i != e; ++i)
            mUI->cbBranch->addItem(i->label());

    if(Table::INVALID_ID != book.id()) {
        mUI->leTitle->setText(mBook.title());
        mUI->leAuthors->setText(mBook.authors());
        mUI->lePublisher->setText(mBook.publisher());
        mUI->leCodes->setText(mBook.codes());
        mUI->sbPages->setValue(mBook.pages());
        auto d = QDate::currentDate();
        d.setDate(mBook.year(), d.month(), d.day());
        mUI->deYear->setDate(d);

        for(auto i = mTags.begin(), e = mTags.end(); i != e; ++i)

            if(i->id() == mBook.tagId()) {
                mUI->cbBranch->setCurrentIndex(std::distance(mTags.begin(), i));
                break;
            }
    } else mUI->deYear->setDate(QDate::currentDate());
    //Tab focus order
    QWidget::setTabOrder(mUI->cbBranch, mUI->leTitle);
    QWidget::setTabOrder(mUI->leTitle, mUI->leAuthors);
    QWidget::setTabOrder(mUI->leAuthors, mUI->deYear);
    QWidget::setTabOrder(mUI->deYear, mUI->lePublisher);
    QWidget::setTabOrder(mUI->lePublisher, mUI->leCodes);
    QWidget::setTabOrder(mUI->leCodes, mUI->sbPages);
    QWidget::setTabOrder(mUI->sbPages, mUI->buttonBox);
}

BookDialog::~BookDialog()
{
    delete mUI;
}

void BookDialog::on_buttonBox_accepted()
{
    auto title = mUI->leTitle->text();
    auto authors = mUI->leAuthors->text();
    auto publisher = mUI->lePublisher->text();
    auto codes = mUI->leCodes->text();
    mBook.setTitle(title);
    mBook.setYear(mUI->deYear->date().year());
    mBook.setAuthors(authors);
    mBook.setPublisher(publisher);
    mBook.setCodes(codes);
    mBook.setPages(mUI->sbPages->value());
    auto i = mTags.begin();
    std::advance(i, mUI->cbBranch->currentIndex());
    mBook.setTagId(i->id());

    if(Table::INVALID_ID == mBook.id() && !mDB.add(mBook))
        qDebug() << "Book insertion failed!";
    else if(!mDB.update(mBook))
        qDebug() << "Book update failed!";
}
