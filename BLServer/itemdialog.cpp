#include "itemdialog.h"
#include "ui_itemdialog.h"

ItemDialog::ItemDialog(DatabaseHandler &db, Item &item, QWidget *parent) :
    QDialog{parent},
    mUI{new Ui::ItemDialog},
    mDB{db},
    mItem{item}
{
    mUI->setupUi(this);

    if(mDB.books(mBooks)) {

        for(auto i = mBooks.begin(), e = mBooks.end(); i != e; ++i) {
            mUI->cbClass->addItem(i->title());
        }
    }

    if(Table::INVALID_ID != item.id()) {
        mUI->leCode->setText(item.inventoryCode());

        for(auto i = mBooks.begin(), e = mBooks.end(); i != e; ++i) {

            if(i->id() == item.bookId())
                mUI->cbClass->setCurrentIndex(std::distance(mBooks.begin(), i));
        }
    }
    //Tab focus order
    QWidget::setTabOrder(mUI->cbClass, mUI->leCode);
    QWidget::setTabOrder(mUI->leCode, mUI->buttonBox);
}

ItemDialog::~ItemDialog()
{
    delete mUI;
}

void ItemDialog::on_ItemDialog_accepted()
{
    auto code = mUI->leCode->text();
    mItem.setInventoryCode(code);
    auto i = mBooks.begin();
    std::advance(i, mUI->cbClass->currentIndex());
    mItem.setBookId(i->id());

    if(Table::INVALID_ID == mItem.id()) {

        if(!mDB.add(mItem)) {
            qDebug() << "Item insertion failed!";
        }
    } else {

        if(!mDB.update(mItem)) {
            qDebug() << "Item update failed!";
        }
    }
}
