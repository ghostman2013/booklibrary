#ifndef CONTEXTMENU_H
#define CONTEXTMENU_H

#include <QMenu>
#include <QAction>
#include "utils/template.h"

class ContextMenu : public QMenu
{
public:
    enum Action {
        Edit = 0xF,
        Remove = 0xF0
    };

    explicit ContextMenu(const QString &title,
                         int action,
                         QWidget *parent = 0);
    void exec();

    template <TEMPLATE_SUBCLASS(QWidget, T)>
    static void show(int count, void (T::*menuListener)(QAction *),
                     T *parent) {

        if(0 < count) {
            int flags = ContextMenu::Remove;

            if(1 == count) { flags |= ContextMenu::Edit; }

            ContextMenu menu("ContextMenu", flags, parent);
            connect(&menu, &ContextMenu::triggered, parent, menuListener);
            menu.exec();
        }
    }
};

#endif // CONTEXTMENU_H
