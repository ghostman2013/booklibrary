#include "contextmenu.h"

ContextMenu::ContextMenu(const QString &title, int action, QWidget *parent):
    QMenu{title, parent}
{
    if(action & Edit) {
        auto a = new QAction("Edit", this);
        a->setData(Edit);
        addAction(a);
    }

    if(action & Remove) {
        auto a = new QAction("Remove", this);
        a->setData(Remove);
        addAction(a);
    }
}

void ContextMenu::exec()
{
    QMenu::exec(QCursor::pos());
}
