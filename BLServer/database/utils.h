#ifndef UTILS_H
#define UTILS_H

#include <QString>
#include <QDate>
#include <database/table.h>
#include <type_traits>

class Utils final
{
private:
    Utils();
public:
    enum class DateField {
        Day,
        Month,
        Year
    };

    static QString md5(const QString& inStr);
    static quint32 pack(const QDate &date);
    static qint32 unpack(quint32 date, DateField field);
    static QDate unpack(quint32 date);

    template<typename E>
    static constexpr auto toUnderlyingType(E e)
        -> typename std::underlying_type<E>::type
    {
        return static_cast<typename std::underlying_type<E>::type>(e);
    }
};

#endif // UTILS_H
