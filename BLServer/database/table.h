#ifndef TABLE_H
#define TABLE_H

#include <QString>

/*!
 * \brief The Table class can use to extend classes
 *        that describe table with ID column.
 */
class Table
{
public:
    /*!
     * \brief INVALID_ID Defines invalid row ID value
     */
    static const qint32 INVALID_ID = -1;
    /*!
     * \brief KEY_ID Defines column 'ID' name
     */
    static const QString KEY_ID;
    /*!
     * \brief Table Constructor
     * \param id Row ID
     */
    Table(qint32 id);
    /*!
     * \brief id Returns row ID
     * \return Row ID value
     */
    qint32 id() const;
    /*!
     * \brief setId Sets row ID
     * \param id Row ID value
     */
    void setId(qint32 id);

private:
    qint32 mId; //Contains row ID value
};

/*!
 * \brief The User class describes user table columns
 *        and can represents its row values.
 */
class User: public Table {
public:
    enum class Role: quint8 {
        Reader,
        Admin
    };
    /*!
     * \brief TABLE_NAME Defines user table name
     */
    static const QString TABLE_NAME;
    /*!
     * \brief KEY_E_MAIL Defines user e-mail column name
     */
    static const QString KEY_E_MAIL;
    /*!
     * \brief KEY_PASSWORD Defines user password column name
     */
    static const QString KEY_PASSWORD;
    /*!
     * \brief KEY_NAME Defines user name column name
     */
    static const QString KEY_NAME;
    /*!
     * \brief KEY_BIRTHDATE Defines user birthdate column name
     */
    static const QString KEY_BIRTHDATE;
    /*!
     * \brief KEY_ROLE Defines role ID column name
     */
    static const QString KEY_ROLE;
    /*!
     * \brief CREATION_SCRIPT Describes table parameters to create it
     */
    static const QString CREATION_SCRIPT;
    /*!
     * \brief User Constructor
     * \param id User row ID value (default INVALID_ID)
     */
    User(qint32 id = INVALID_ID);
    /*!
     * \brief eMail Returns user e-mail
     * \return E-mail value
     */
    const QString &eMail() const;
    /*!
     * \brief password Returns user hashed password
     * \return Hashed password value
     */
    const QString &password() const;
    /*!
     * \brief name Returns user name
     * \return Name value
     */
    const QString &name() const;
    /*!
     * \brief birthdate Returns user birthdate
     * \return Birthdate value (Unix format)
     */
    quint32 birthdate() const;
    /*!
     * \brief role Returns user role
     * \return Role value
     */
    Role role() const;
    User &setEMail(const QString& eMail);
    User &setPassword(const QString& password);
    User &setName(const QString& name);
    User &setBirthdate(quint32 birthdate);
    User &setRole(Role role);
private:
    QString mEMail; //Constains user e-mail
    QString mPassword; //Contains user password's hash
    QString mName; //Contains user name
    quint32 mDate; //Contains user birthdate
    Role mRole; //Contains user role (access level)
};

/*!
 * \brief The Tag class describes tag table columns
 *        and can represents its row value.
 */
class Tag: public Table {
public:
    /*!
     * \brief TABLE_NAME Defines tag table name
     */
    static const QString TABLE_NAME;
    static const QString KEY_LABEL;
    static const QString KEY_PARENT_ID;
    /*!
     * \brief CREATION_SCRIPT Describes table parameters to create it
     */
    static const QString CREATION_SCRIPT;
    /*!
     * \brief Tag Constructor
     * \param id Tag row ID value (default INVALID_ID)
     */
    Tag(qint32 id = INVALID_ID);
    qint32 parentId() const;
    const QString &label() const;
    Tag &setParentId(qint32 parentId);
    Tag &setLabel(const QString &label);
private:
    qint32 mParent;
    QString mLabel;
};

/*!
 * \brief The Book class describes book table columns
 *        and can represents its row value.
 */
class Book: public Table {
public:
    /*!
     * \brief TABLE_NAME Defines book table name
     */
    static const QString TABLE_NAME;
    static const QString KEY_TITLE;
    static const QString KEY_PAGES;
    static const QString KEY_YEAR;
    static const QString KEY_PUBLISHER;
    static const QString KEY_AUTHORS;
    static const QString KEY_CODES;
    static const QString KEY_TAG_ID;
    /*!
     * \brief CREATION_SCRIPT Describes table parameters to create it
     */
    static const QString CREATION_SCRIPT;
    /*!
     * \brief Book Constructor
     * \param id Book row ID value (default INVALID_ID)
     */
    Book(qint32 id = INVALID_ID);
    const QString &title() const;
    quint16 pages() const;
    quint16 year() const;
    const QString &publisher() const;
    const QString &authors() const;
    const QString &codes() const;
    qint32 tagId() const;
    Book &setTitle(const QString &title);
    Book &setPages(quint16 pages);
    Book &setYear(quint16 year);
    Book &setPublisher(const QString &publisher);
    Book &setAuthors(const QString &authors);
    Book &setCodes(const QString &codes);
    Book &setTagId(qint32 tagId);
private:
    QString mTitle;
    quint16 mPages;
    quint16 mYear;
    QString mPublisher;
    QString mAuthors;
    QString mCodes;
    qint32 mTag;
};

/*!
 * \brief The Item class describes item table columns
 *        and can represents its row value.
 */
class Item: public Table {
public:
    /*!
     * \brief TABLE_NAME Describes item table name
     */
    static const QString TABLE_NAME;
    /*!
     * \brief KEY_INVENTORY_CODE Describes iventory code column name
     */
    static const QString KEY_INVENTORY_CODE;
    /*!
     * \brief KEY_BOOK_ID Describes Book ID column name
     */
    static const QString KEY_BOOK_ID;
    /*!
     * \brief CREATION_SCRIPT Describes table parameters to create it
     */
    static const QString CREATION_SCRIPT;
    /*!
     * \brief Item Constructor
     * \param id Item row ID value (default INVALID_ID)
     */
    Item(qint32 id = INVALID_ID);
    /*!
     * \brief inventoryCode Returns item inventory code
     * \return The inventory code value
     */
    const QString& inventoryCode() const;
    /*!
     * \brief bookId Returns Book ID
     * \return Book ID value
     */
    qint32 bookId() const;
    /*!
     * \brief setInventoryCode Sets item inventory code
     * \param inventoryCode The inventory code value
     * \return Self
     */
    Item &setInventoryCode(const QString& inventoryCode);
    /*!
     * \brief setBookId Sets Book ID
     * \param bookId Book ID value
     * \return
     */
    Item &setBookId(qint32 bookId);
private:
    QString mCode; //Contains item inventory code value
    qint32 mBook; //Contains Book ID value
};

/*!
 * \brief The Log class describes log table columns
 *        and can represents its row value.
 */
class Log: public Table {
public:
    /*!
     * \brief The Action enum contains possible action variants
     *        that can be applied to real book (item)
     */
    enum class Action: quint16 {
        Issuance,
        Return,
        Request
    };
    /*!
     * \brief TABLE_NAME Describes log table name
     */
    static const QString TABLE_NAME;
    /*!
     * \brief KEY_TIMESTAMP Describes timestamp column name
     */
    static const QString KEY_TIMESTAMP;
    /*!
     * \brief KEY_ACTION Descibes action column name
     */
    static const QString KEY_ACTION;
    /*!
     * \brief KEY_ITEM_ID Describes Item ID column name
     */
    static const QString KEY_ITEM_ID;
    /*!
     * \brief KEY_USER_ID Describes User ID column name
     */
    static const QString KEY_USER_ID;
    /*!
     * \brief CREATION_SCRIPT Describes table parameters to create it
     */
    static const QString CREATION_SCRIPT;
    /*!
     * \brief Log Constructor
     * \param id Log row ID value (default INVALID_ID)
     */
    Log(qint32 id = INVALID_ID);
    /*!
     * \brief timestamp Returns timestamp for log row
     * \return The row timestamp
     */
    quint64 timestamp() const;
    /*!
     * \brief action Returns action type
     * \return The action type (ISSUANCE | RETURN)
     */
    Action action() const;
    /*!
     * \brief itemId Returns Item ID
     * \return Item ID value
     */
    qint32 itemId() const;
    /*!
     * \brief userId Returns User ID
     * \return User ID value
     */
    qint32 userId() const;
    /*!
     * \brief setTimestamp Sets timestamp for log row
     * \param timestamp The row timestamp
     * \return Self
     */
    Log &setTimestamp(quint64 timestamp);
    /*!
     * \brief setAction Sets action type
     * \param action The action type (ISSUANCE | RETURN)
     * \return Self
     */
    Log &setAction(Action action);
    /*!
     * \brief setItemId Sets the Item ID
     * \param itemId Item ID value
     * \return Self
     */
    Log &setItemId(qint32 itemId);
    /*!
     * \brief setUserId Sets the User ID
     * \param userId User ID value
     * \return Self
     */
    Log &setUserId(qint32 userId);
private:
    quint64 mTime; //Constains row timestamp
    Action mAction; //Constains action type
    qint32 mItem; //Constains Item ID value
    qint32 mUser; //Constains User ID value
};

#endif // TABLE_H
