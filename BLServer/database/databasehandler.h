#ifndef DATABASEHANDLER_H
#define DATABASEHANDLER_H
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlQueryModel>
#include <QDebug>
#include <list>
#include "table.h"
#include "utils.h"


class DatabaseHandler
{
public:
    static const QString DEFAULT_DB_NAME;
    static const quint16 DEFAULT_DB_PORT = 3306U;
    static const QString DEFAULT_DB_HOST;
    DatabaseHandler(const QString &database = DEFAULT_DB_NAME,
                    quint16 port = DEFAULT_DB_PORT,
                    const QString &host = DEFAULT_DB_HOST);
    ~DatabaseHandler();
    bool connect(const QString &username, const QString &password);
    bool create();
    bool drop();
    void close();
    //Insertion
    bool add(User &user);
    bool add(Tag &tag);
    bool add(Book &book);
    bool add(Item &item);
    bool add(Log &log);
    //Update
    bool update(const User &user);
    bool update(const Tag &tag);
    bool update(const Book &book);
    bool update(const Item &item);
    //Selection
    bool users(std::list<User> &users);
    bool user(User &user, qint32 id);
    bool user(User &user, const QString &email, const QString &password);
    bool tags(std::list<Tag> &tags);
    bool tags(std::list<Tag> &tags, qint32 parentId);
    bool tag(Tag &tag, const QString &label);
    bool books(std::list<Book> &books);
    bool books(std::list<Book> &books, qint32 tagId);
    bool items(std::list<Item> &items, qint32 bookId);
    bool item(Item &item, qint32 id);
    bool logs(std::list<Log> &logs);
    //Removing
    bool removeTag(const QString &label);
    bool removeBook(qint32 id);
    bool removeItem(qint32 id);
    bool removeUser(qint32 id);
private:
    bool add(QSqlQuery &query, Table &row);
    bool update(QSqlQuery &query, const Table &row);
    bool tagsCommon(std::list<Tag> &tags, const QString &sql);
    bool booksCommon(std::list<Book> &books, const QString &sql);
    bool remove(QSqlQuery &query);
    static inline QString c(const QString &columnName);
    static inline QString eq(const QString &column, const QString &value);
    static inline void log(const QString &sql);
    QSqlDatabase mDB;
};

#endif // DATABASEHANDLER_H
