#include "table.h"

/******************************************************************************
* Table class                                                                 *
*******************************************************************************/

/******************************************************************************
* Constants                                                                   *
*******************************************************************************/

const QString Table::KEY_ID = "id";

/******************************************************************************
 * Constructors & Destructors                                                 *
 ******************************************************************************/

Table::Table(qint32 id):
    mId{id}
{
}

/******************************************************************************
 * Methods                                                                    *
 ******************************************************************************/

qint32 Table::id() const
{
    return mId;
}

void Table::setId(qint32 id) {
    this->mId = id;
}

/******************************************************************************
* User class                                                                  *
*******************************************************************************/

/******************************************************************************
* Constants                                                                   *
*******************************************************************************/

const QString User::TABLE_NAME = "users";
const QString User::KEY_E_MAIL = "email";
const QString User::KEY_PASSWORD = "password";
const QString User::KEY_NAME = "name";
const QString User::KEY_BIRTHDATE = "birthdate";
const QString User::KEY_ROLE = "role_id";
const QString User::CREATION_SCRIPT = "CREATE TABLE IF NOT EXISTS `"
        + User::TABLE_NAME + "` (`"
        + User::KEY_ID + "` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `"
        + User::KEY_E_MAIL + "` NVARCHAR(320) NOT NULL, `"
        + User::KEY_PASSWORD + "` CHAR(32) NOT NULL, `"
        + User::KEY_NAME + "` NVARCHAR(256) NOT NULL, `"
        + User::KEY_BIRTHDATE + "` INT(10) UNSIGNED NOT NULL, `"
        + User::KEY_ROLE + "` TINYINT(3) UNSIGNED NOT NULL);";

/******************************************************************************
 * Constructors & Destructors                                                 *
 ******************************************************************************/

User::User(qint32 id):
    Table{id}
{
    //Nothing to do
}

/******************************************************************************
 * Methods                                                                    *
 ******************************************************************************/

const QString &User::eMail() const
{
    return mEMail;
}

const QString &User::password() const
{
    return mPassword;
}

const QString &User::name() const
{
    return mName;
}

quint32 User::birthdate() const
{
    return mDate;
}

User::Role User::role() const
{
    return mRole;
}

User &User::setEMail(const QString &eMail)
{
    mEMail = eMail;
    return *this;
}

User &User::setPassword(const QString &password)
{
    mPassword = password;
    return *this;
}

User &User::setName(const QString &name)
{
    mName = name;
    return *this;
}

User &User::setBirthdate(quint32 birthdate)
{
    mDate = birthdate;
    return *this;
}

User &User::setRole(Role role)
{
    mRole = role;
    return *this;
}

/******************************************************************************
* Tag class                                                                   *
*******************************************************************************/

/******************************************************************************
* Constants                                                                   *
*******************************************************************************/

const QString Tag::TABLE_NAME = "tags";
const QString Tag::KEY_LABEL = "label";
const QString Tag::KEY_PARENT_ID = "parent_id";
const QString Tag::CREATION_SCRIPT = "CREATE TABLE IF NOT EXISTS `"
        + Tag::TABLE_NAME + "` (`"
        + Tag::KEY_ID + "` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `"
        + Tag::KEY_LABEL + "` NVARCHAR(128) NOT NULL, `"
        + Tag::KEY_PARENT_ID + "` INT(11) NULL,"
        + "FOREIGN KEY (`" + Tag::KEY_PARENT_ID + "`) REFERENCES `"
        + Tag::TABLE_NAME + "` (`" + Tag::KEY_ID + "`)"
        + " ON DELETE CASCADE ON UPDATE CASCADE, "
        + "UNIQUE (`" + Tag::KEY_LABEL + "`));";

/******************************************************************************
 * Constructors & Destructors                                                 *
 ******************************************************************************/

Tag::Tag(qint32 id):
    Table{id},
    mParent{Table::INVALID_ID}
{
    //Nothing to do
}

/******************************************************************************
 * Methods                                                                    *
 ******************************************************************************/

qint32 Tag::parentId() const
{
    return mParent;
}

const QString &Tag::label() const
{
    return mLabel;
}

Tag &Tag::setParentId(qint32 parentId)
{
    mParent = parentId;
    return *this;
}

Tag &Tag::setLabel(const QString &label)
{
    mLabel = label;
    return *this;
}

/******************************************************************************
* Book class                                                                  *
*******************************************************************************/

/******************************************************************************
* Constants                                                                   *
*******************************************************************************/

const QString Book::TABLE_NAME = "books";
const QString Book::KEY_TITLE = "title";
const QString Book::KEY_PAGES = "pages";
const QString Book::KEY_YEAR = "year";
const QString Book::KEY_PUBLISHER = "publisher";
const QString Book::KEY_AUTHORS = "authors";
const QString Book::KEY_CODES = "codes";
const QString Book::KEY_TAG_ID = "tag_id";
const QString Book::CREATION_SCRIPT = "CREATE TABLE IF NOT EXISTS `"
        + Book::TABLE_NAME + "` (`"
        + Book::KEY_ID + "` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `"
        + Book::KEY_TITLE + "` NVARCHAR(256) NOT NULL, `"
        + Book::KEY_PAGES + "` MEDIUMINT(8) UNSIGNED NOT NULL, `"
        + Book::KEY_YEAR + "` MEDIUMINT(8) UNSIGNED NOT NULL, `"
        + Book::KEY_PUBLISHER + "` NVARCHAR(256) NOT NULL, `"
        + Book::KEY_AUTHORS + "` NVARCHAR(256) NOT NULL, `"
        + Book::KEY_CODES + "` NVARCHAR(128) NOT NULL, `"
        + Book::KEY_TAG_ID + "` INT(11) NOT NULL,"
        + "FOREIGN KEY (`" + Book::KEY_TAG_ID + "`) REFERENCES `"
        + Tag::TABLE_NAME + "` (`" + Tag::KEY_ID + "`)"
        + " ON DELETE CASCADE ON UPDATE CASCADE);";

/******************************************************************************
 * Constructors & Destructors                                                 *
 ******************************************************************************/

Book::Book(qint32 id):
    Table{id}
{
    //Nothing to do
}

/******************************************************************************
 * Methods                                                                    *
 ******************************************************************************/

const QString &Book::title() const
{
    return mTitle;
}

quint16 Book::pages() const
{
    return mPages;
}

quint16 Book::year() const
{
    return mYear;
}

const QString &Book::publisher() const
{
    return mPublisher;
}

const QString &Book::authors() const
{
    return mAuthors;
}

const QString &Book::codes() const
{
    return mCodes;
}

qint32 Book::tagId() const
{
    return mTag;
}

Book &Book::setTitle(const QString &title)
{
    mTitle= title;
    return *this;
}

Book &Book::setPages(quint16 pages)
{
    mPages = pages;
    return *this;
}

Book &Book::setYear(quint16 year)
{
    mYear = year;
    return *this;
}

Book &Book::setPublisher(const QString &publisher)
{
    mPublisher = publisher;
    return *this;
}

Book &Book::setAuthors(const QString &authors)
{
    mAuthors = authors;
    return *this;
}

Book &Book::setCodes(const QString &codes)
{
    mCodes = codes;
    return *this;
}

Book &Book::setTagId(qint32 tagId) {
    mTag = tagId;
    return *this;
}

/******************************************************************************
* Item class                                                                  *
*******************************************************************************/

/******************************************************************************
* Constants                                                                   *
*******************************************************************************/

const QString Item::TABLE_NAME = "items";
const QString Item::KEY_INVENTORY_CODE = "inv_code";
const QString Item::KEY_BOOK_ID = "book_id";
const QString Item::CREATION_SCRIPT = "CREATE TABLE IF NOT EXISTS `"
        + Item::TABLE_NAME + "` (`"
        + Item::KEY_ID + "` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `"
        + Item::KEY_INVENTORY_CODE + "` NVARCHAR(32) NOT NULL, `"
        + Item::KEY_BOOK_ID + "` INT(11) NOT NULL, "
        + "FOREIGN KEY (`" + Item::KEY_BOOK_ID + "`) REFERENCES `"
        + Book::TABLE_NAME + "` (`" + Book::KEY_ID + "`) "
        + " ON DELETE CASCADE ON UPDATE CASCADE, "
        + "UNIQUE(`" + Item::KEY_INVENTORY_CODE + "`));";

/******************************************************************************
 * Constructors & Destructors                                                 *
 ******************************************************************************/

Item::Item(qint32 id):
    Table{id}
{
    //Nothing to do
}

/******************************************************************************
 * Methods                                                                    *
 ******************************************************************************/

const QString &Item::inventoryCode() const
{
    return mCode;
}

qint32 Item::bookId() const
{
    return mBook;
}

Item &Item::setInventoryCode(const QString &inventoryCode)
{
    mCode = inventoryCode;
    return *this;
}

Item &Item::setBookId(qint32 bookId) {
    mBook = bookId;
    return *this;
}

/******************************************************************************
* Log class                                                                   *
*******************************************************************************/

/******************************************************************************
* Constants                                                                   *
*******************************************************************************/

const QString Log::TABLE_NAME = "log";
const QString Log::KEY_TIMESTAMP = "timestamp";
const QString Log::KEY_ACTION = "action";
const QString Log::KEY_ITEM_ID = "item_id";
const QString Log::KEY_USER_ID = "user_id";
const QString Log::CREATION_SCRIPT = "CREATE TABLE IF NOT EXISTS `"
        + Log::TABLE_NAME + "` (`"
        + Log::KEY_ID + "` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `"
        + Log::KEY_TIMESTAMP + "` TIMESTAMP(6) NOT NULL, `"
        + Log::KEY_ACTION + "` MEDIUMINT(8) UNSIGNED NOT NULL, `"
        + Log::KEY_ITEM_ID + "` INT(11) NOT NULL, `"
        + Log::KEY_USER_ID + "` INT(11) NOT NULL, "
        + "FOREIGN KEY (`" + Log::KEY_ITEM_ID + "`) REFERENCES `"
        + Item::TABLE_NAME + "` (`" + Item::KEY_ID + "`) "
        + " ON DELETE CASCADE ON UPDATE CASCADE, "
        + "FOREIGN KEY (`" + Log::KEY_USER_ID + "`) REFERENCES `"
        + User::TABLE_NAME + "` (`" + User::KEY_ID + "`) "
        + " ON DELETE CASCADE ON UPDATE CASCADE);";

/******************************************************************************
 * Constructors & Destructors                                                 *
 ******************************************************************************/

Log::Log(qint32 id):
    Table{id}
{
    //Nothing to do
}

/******************************************************************************
 * Methods                                                                    *
 ******************************************************************************/

quint64 Log::timestamp() const
{
    return mTime;
}

Log::Action Log::action() const
{
    return mAction;
}

qint32 Log::itemId() const
{
    return mItem;
}

qint32 Log::userId() const
{
    return mUser;
}

Log &Log::setTimestamp(quint64 timestamp)
{
    mTime = timestamp;
    return *this;
}

Log &Log::setAction(Log::Action action)
{
    mAction = action;
    return *this;
}

Log &Log::setItemId(qint32 itemId)
{
    mItem = itemId;
    return *this;
}

Log &Log::setUserId(qint32 userId)
{
    mUser = userId;
    return *this;
}
