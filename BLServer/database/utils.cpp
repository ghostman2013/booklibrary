#include "utils.h"
#include <QCryptographicHash>

Utils::Utils()
{

}

QString Utils::md5(const QString &inStr)
{
    auto out = QCryptographicHash::hash(inStr.toUtf8(),
                                        QCryptographicHash::Md5).toHex();
    return QString(out);
}

quint32 Utils::pack(const QDate &date)
{
    quint32 d = date.year();
    d = (d << 4) + date.month();
    d = (d << 8) + date.day();
    return d;
}

qint32 Utils::unpack(quint32 date, Utils::DateField field)
{
    switch(field) {
    case DateField::Day:
        return date & 0xFF;
    case DateField::Month:
        return (date >> 8) & 0xF;
    case DateField::Year:
        return date >> 12;
    }

    return -1;
}

QDate Utils::unpack(quint32 date)
{
    int year = unpack(date, DateField::Year);
    int month = unpack(date, DateField::Month);
    int day = unpack(date, DateField::Day);
    QDate d(year, month, day);
    return d;
}
