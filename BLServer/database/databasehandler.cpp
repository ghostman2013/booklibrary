#include "databasehandler.h"
#define COM ", "

const QString DatabaseHandler::DEFAULT_DB_NAME = "BookLibrary";
const QString DatabaseHandler::DEFAULT_DB_HOST = "127.0.0.1";

DatabaseHandler::DatabaseHandler(const QString &database,
                                 quint16 port,
                                 const QString &host)
{
    mDB = QSqlDatabase::addDatabase("QMYSQL");
    mDB.setDatabaseName(database);
    mDB.setPort(port);
    mDB.setHostName(host);
}

DatabaseHandler::~DatabaseHandler()
{
    mDB.close();
}

bool DatabaseHandler::connect(const QString &username, const QString &password)
{
    mDB.setUserName(username);
    mDB.setPassword(password);

    if(mDB.open()) {
        return true;
    }

    return false;
}

bool DatabaseHandler::create()
{
    QSqlQuery query(mDB);
    mDB.transaction();
    bool result = true;

    if(!query.exec(User::CREATION_SCRIPT)) {
        log(User::CREATION_SCRIPT);
        result = false;
    }

    if(!query.exec(Tag::CREATION_SCRIPT)) {
        log(Tag::CREATION_SCRIPT);
        result = false;
    }

    if(!query.exec(Book::CREATION_SCRIPT)) {
        log(Book::CREATION_SCRIPT);
        result = false;
    }

    if(!query.exec(Item::CREATION_SCRIPT)) {
        log(Item::CREATION_SCRIPT);
        result = false;
    }

    if(!query.exec(Log::CREATION_SCRIPT)) {
        log(Log::CREATION_SCRIPT);
        result = false;
    }

    if(result) {
        mDB.commit();
        return true;
    } else {
        mDB.rollback();
        return false;
    }

}

bool DatabaseHandler::drop()
{
    QSqlQuery query(mDB);
    mDB.transaction();
    bool result = true;

    if(!query.exec("DROP TABLE " + Log::TABLE_NAME + ";"))
        result = false;

    if(!query.exec("DROP TABLE " + Item::TABLE_NAME + ";"))
        result = false;

    if(!query.exec("DROP TABLE " + User::TABLE_NAME + ";"))
        result = false;

    if(!query.exec("DROP TABLE " + Book::TABLE_NAME + ";"))
        result = false;

    if(!query.exec("DROP TABLE " + Tag::TABLE_NAME + ";"))
        result = false;

    if(result) {
        mDB.commit();
        return true;
    } else {
        mDB.rollback();
        return false;
    }
}

void DatabaseHandler::close()
{
    mDB.close();
}

bool DatabaseHandler::add(User &user)
{
    const QString sql = "INSERT INTO " + c(User::TABLE_NAME) + " ("
            + c(User::KEY_E_MAIL) + COM
            + c(User::KEY_PASSWORD) + COM
            + c(User::KEY_NAME) + COM
            + c(User::KEY_BIRTHDATE) + COM
            + c(User::KEY_ROLE) + ") VALUES ( :eMail, :Password, :Name, "
            + QString::number(user.birthdate()) + COM
            + QString::number((quint8)user.role()) + ");";
    QSqlQuery query(mDB);
    query.prepare(sql);
    query.bindValue(":eMail", user.eMail());
    query.bindValue(":Password", user.password());
    query.bindValue(":Name", user.name());
    return add(query, user);
}

bool DatabaseHandler::add(Tag &tag)
{
    QString sql = "INSERT INTO " + c(Tag::TABLE_NAME) + " ("
            + c(Tag::KEY_LABEL);

    if(tag.parentId() != Table::INVALID_ID) {
        sql += COM + c(Tag::KEY_PARENT_ID)
                + ") VALUES ( :Label , "
                + QString::number(tag.parentId());
    } else {
        sql += ") VALUES ( :Label ";
    }

    sql += ");";

    QSqlQuery query(mDB);
    query.prepare(sql);
    query.bindValue(":Label", tag.label());
    return add(query, tag);
}

bool DatabaseHandler::add(Book &book)
{
    const QString sql = "INSERT INTO " + c(Book::TABLE_NAME) + " ("
            + c(Book::KEY_TITLE) + COM
            + c(Book::KEY_PAGES) + COM
            + c(Book::KEY_YEAR) + COM
            + c(Book::KEY_PUBLISHER) + COM
            + c(Book::KEY_AUTHORS) + COM
            + c(Book::KEY_CODES) + COM
            + c(Book::KEY_TAG_ID) + ") VALUES (:Title, "
            + QString::number(book.pages()) + COM
            + QString::number(book.year()) + ", :Publisher, :Authors, :Codes, "
            + QString::number(book.tagId()) + ");";
    QSqlQuery query(mDB);
    query.prepare(sql);
    query.bindValue(":Title", book.title());
    query.bindValue(":Publisher", book.publisher());
    query.bindValue(":Authors", book.authors());
    query.bindValue(":Codes", book.codes());
    return add(query, book);
}

bool DatabaseHandler::add(Item &item)
{
    const QString sql = "INSERT INTO " + c(Item::TABLE_NAME) + " ("
            + c(Item::KEY_INVENTORY_CODE) + COM
            + c(Item::KEY_BOOK_ID) + ") VALUES ( :Code, "
            + QString::number(item.bookId()) + ");";
    QSqlQuery query(mDB);
    query.prepare(sql);
    query.bindValue(":Code", item.inventoryCode());
    return add(query, item);
}

bool DatabaseHandler::add(Log &log)
{
    const QString sql = "INSERT INTO " + c(Log::TABLE_NAME) + " ("
            + c(Log::KEY_ACTION) + COM
            + c(Log::KEY_ITEM_ID) + COM
            + c(Log::KEY_USER_ID) + ") VALUES ("
            + QString::number((quint16)log.action()) + COM
            + QString::number(log.itemId()) + COM
            + QString::number(log.userId()) + ");";
    QSqlQuery query(mDB);
    query.prepare(sql);
    return add(query, log);
}

bool DatabaseHandler::update(const User &user)
{
    const QString sql = "UPDATE " + c(User::TABLE_NAME) + " SET "
            + eq(User::KEY_E_MAIL, ":Email") + COM
            + eq(User::KEY_PASSWORD, ":Password") + COM
            + eq(User::KEY_NAME, ":Name") + COM
            + eq(User::KEY_BIRTHDATE, QString::number(user.birthdate())) + COM
            + eq(User::KEY_ROLE, QString::number((int)user.role()))
            + " WHERE " + eq(Table::KEY_ID, QString::number(user.id())) + ";";
    QSqlQuery query(mDB);
    query.prepare(sql);
    query.bindValue(":Email", user.eMail());
    query.bindValue(":Password", user.password());
    query.bindValue(":Name", user.name());
    return update(query, user);
}

bool DatabaseHandler::update(const Tag &tag)
{
    const QString parent = Table::INVALID_ID == tag.parentId() ?
                "NULL" : QString::number(tag.parentId());
    const QString sql = "UPDATE " + c(Tag::TABLE_NAME) + " SET "
            + eq(Tag::KEY_LABEL, ":Label") + COM
            + eq(Tag::KEY_PARENT_ID, parent)
            + " WHERE " + eq(Table::KEY_ID, QString::number(tag.id()));
    QSqlQuery query(mDB);
    query.prepare(sql);
    query.bindValue(":Label", tag.label());
    return update(query, tag);
}

bool DatabaseHandler::update(const Book &book)
{
    const QString sql = "UPDATE " + c(Book::TABLE_NAME) + " SET "
            + eq(Book::KEY_TITLE, ":Title") + COM
            + eq(Book::KEY_PAGES, QString::number(book.pages())) + COM
            + eq(Book::KEY_YEAR, QString::number(book.year())) + COM
            + eq(Book::KEY_PUBLISHER, ":Publisher") + COM
            + eq(Book::KEY_AUTHORS, ":Authors") + COM
            + eq(Book::KEY_CODES, ":Codes") + COM
            + eq(Book::KEY_TAG_ID, QString::number(book.tagId()))
            + " WHERE " + eq(Table::KEY_ID, QString::number(book.id())) + ";";
    QSqlQuery query(mDB);
    query.prepare(sql);
    query.bindValue(":Title", book.title());
    query.bindValue(":Publisher", book.publisher());
    query.bindValue(":Authors", book.authors());
    query.bindValue(":Codes", book.codes());
    return update(query, book);
}

bool DatabaseHandler::update(const Item &item)
{
    const QString sql = "UPDATE " + c(Item::TABLE_NAME) + " SET "
            + eq(Item::KEY_INVENTORY_CODE, ":Code") + COM
            + eq(Item::KEY_BOOK_ID, QString::number(item.bookId()))
            + " WHERE " + eq(Table::KEY_ID, QString::number(item.id()));
    QSqlQuery query(mDB);
    query.prepare(sql);
    query.bindValue(":Code", item.inventoryCode());
    return update(query, item);
}

bool DatabaseHandler::users(std::list<User> &users)
{
    QString sql = "SELECT * FROM " + c(User::TABLE_NAME) + ";";
    QSqlQueryModel model;
    model.setQuery(sql);

    if(model.lastError().type() != QSqlError::NoError) {
        log(sql);
        return false;
    }

    const int count = model.rowCount();

    for(int i = 0; i < count; ++i) {
        auto record = model.record(i);
        User user(record.value(Table::KEY_ID).toInt());
        user.setEMail(record.value(User::KEY_E_MAIL).toString());
        user.setPassword(record.value(User::KEY_PASSWORD).toString());
        user.setName(record.value(User::KEY_NAME).toString());
        user.setBirthdate(record.value(User::KEY_BIRTHDATE).toUInt());
        user.setRole(static_cast<User::Role>(record
                                             .value(User::KEY_ROLE).toUInt()));
        users.push_back(user);
    }

    return true;
}

bool DatabaseHandler::user(User &user, qint32 id)
{
    QString sql = "SELECT * FROM " + c(User::TABLE_NAME)
            + " WHERE " + eq(Table::KEY_ID, QString::number(id)) + ";";
    QSqlQueryModel model;
    model.setQuery(sql);

    if(model.lastError().type() != QSqlError::NoError) {
        log(sql);
        return false;
    }

    if(model.rowCount() > 0) {
        auto record = model.record(0);
        user.setId(record.value(Table::KEY_ID).toInt());
        user.setEMail(record.value(User::KEY_E_MAIL).toString());
        user.setPassword(record.value(User::KEY_PASSWORD).toString());
        user.setName(record.value(User::KEY_NAME).toString());
        user.setBirthdate(record.value(User::KEY_BIRTHDATE).toUInt());
        auto role = record.value(User::KEY_ROLE).toUInt();
        user.setRole(static_cast<User::Role>(role));
        return true;
    }

    return false;
}

bool DatabaseHandler::user(User &user,
                           const QString &email,
                           const QString &password)
{
    QString sql = "SELECT * FROM " + c(User::TABLE_NAME)
            + " WHERE " + eq(User::KEY_E_MAIL, ":Email")
            + " AND " + eq(User::KEY_PASSWORD, ":Password") + ";";
    QSqlQuery query(mDB);
    query.prepare(sql);
    query.bindValue(":Email", email);
    query.bindValue(":Password", password);

    if(!query.exec()) {
        log(query.lastQuery());
        return false;
    }

    QSqlQueryModel model;
    model.setQuery(query);

    if(model.lastError().type() != QSqlError::NoError) {
        log(sql);
        return false;
    }

    if(model.rowCount() > 0) {
        auto record = model.record(0);
        user.setId(record.value(Table::KEY_ID).toInt());
        user.setEMail(record.value(User::KEY_E_MAIL).toString());
        user.setPassword(record.value(User::KEY_PASSWORD).toString());
        user.setName(record.value(User::KEY_NAME).toString());
        user.setBirthdate(record.value(User::KEY_BIRTHDATE).toUInt());
        auto role = record.value(User::KEY_ROLE).toUInt();
        user.setRole(static_cast<User::Role>(role));
        return true;
    }

    return false;
}

bool DatabaseHandler::tags(std::list<Tag> &tags)
{
    QString sql = "SELECT * FROM " + c(Tag::TABLE_NAME) + ";";
    return tagsCommon(tags, sql);
}

bool DatabaseHandler::tags(std::list<Tag> &tags, qint32 parentId)
{
    QString sql = "SELECT * FROM " + c(Tag::TABLE_NAME) + " WHERE "
            + eq(Tag::KEY_PARENT_ID, QString::number(parentId)) + ";";
    return tagsCommon(tags, sql);
}

bool DatabaseHandler::tag(Tag &tag, const QString &label)
{
    QString sql = "SELECT * FROM " + c(Tag::TABLE_NAME)
            + " WHERE " + eq(Tag::KEY_LABEL, ":Label") + ";";
    QSqlQuery query(mDB);
    query.prepare(sql);
    query.bindValue(":Label", label);

    if(!query.exec()) {
        log(query.lastQuery());
        return false;
    }

    QSqlQueryModel model;
    model.setQuery(query);

    if(model.lastError().type() != QSqlError::NoError) {
        log(sql);
        return false;
    }

    if(0 < model.rowCount()) {
        auto record = model.record(0);
        tag.setId(record.value(Table::KEY_ID).toInt());
        tag.setLabel(record.value(Tag::KEY_LABEL).toString());
        qint32 parentId = record.isNull(Tag::KEY_PARENT_ID) ?
                    Table::INVALID_ID
                  : record.value(Tag::KEY_PARENT_ID).toInt();
        tag.setParentId(parentId);
        return true;
    }

    return false;
}

bool DatabaseHandler::books(std::list<Book> &books)
{
    QString sql = "SELECT * FROM " + c(Book::TABLE_NAME) + ";";
    return booksCommon(books, sql);
}

bool DatabaseHandler::books(std::list<Book> &books, qint32 tagId)
{
    QString sql = "SELECT * FROM " + c(Book::TABLE_NAME)
            + " WHERE " + eq(Book::KEY_TAG_ID, QString::number(tagId)) + ";";
    return booksCommon(books, sql);
}

bool DatabaseHandler::items(std::list<Item> &items, qint32 bookId)
{
    QString sql = "SELECT * FROM " + c(Item::TABLE_NAME)
            + " WHERE " + eq(Item::KEY_BOOK_ID, QString::number(bookId)) + ";";
    QSqlQueryModel model;
    model.setQuery(sql);

    if(model.lastError().type() != QSqlError::NoError) {
        log(sql);
        return false;
    }

    const int count = model.rowCount();

    for(int i = 0; i < count; ++i) {
        auto record = model.record(i);
        Item item(record.value(Table::KEY_ID).toInt());
        item.setInventoryCode(record.value(Item::KEY_INVENTORY_CODE)
                              .toString());
        qint32 bookId = record.isNull(Item::KEY_BOOK_ID) ?
                    Table::INVALID_ID
                  : record.value(Item::KEY_BOOK_ID).toInt();
        item.setBookId(bookId);
        items.push_back(item);
    }

    return true;
}

bool DatabaseHandler::item(Item &item, qint32 id)
{
    QString sql = "SELECT * FROM " + c(Item::TABLE_NAME)
            + " WHERE " + eq(Table::KEY_ID, QString::number(id)) + ";";
    QSqlQueryModel model;
    model.setQuery(sql);

    if(model.lastError().type() != QSqlError::NoError) {
        log(sql);
        return false;
    }

    if(model.rowCount()) {
        auto record = model.record(0);
        item.setId(record.value(Table::KEY_ID).toInt());
        item.setInventoryCode(record.value(Item::KEY_INVENTORY_CODE)
                              .toString());
        qint32 bookId = record.isNull(Item::KEY_BOOK_ID) ?
                    Table::INVALID_ID
                  : record.value(Item::KEY_BOOK_ID).toInt();
        item.setBookId(bookId);
    }

    return true;
}

bool DatabaseHandler::logs(std::list<Log> &logs)
{
    QString sql = "SELECT * FROM " + c(Log::TABLE_NAME) + ";";
    QSqlQueryModel model;
    model.setQuery(sql);

    if(model.lastError().type() != QSqlError::NoError) {
        log(sql);
        return false;
    }

    const int count = model.rowCount();

    for(int i = 0; i < count; ++i) {
        auto record = model.record(i);
        Log log(record.value(Table::KEY_ID).toInt());
        log.setTimestamp(record.value(Log::KEY_TIMESTAMP)
                         .toDateTime().currentMSecsSinceEpoch());
        auto action = record.value(Log::KEY_ACTION).toUInt();
        log.setAction(static_cast<Log::Action>(action));
        log.setItemId(record.value(Log::KEY_ITEM_ID).toInt());
        log.setUserId(record.value(Log::KEY_USER_ID).toInt());
        logs.push_back(log);
        return true;
    }

    return false;
}

bool DatabaseHandler::removeTag(const QString &label)
{
    const QString sql = "DELETE FROM " + c(Tag::TABLE_NAME) + " WHERE "
            + eq(Tag::KEY_LABEL, ":Label") + ";";
    QSqlQuery query(mDB);
    query.prepare(sql);
    query.bindValue(":Label", label);
    return remove(query);
}

bool DatabaseHandler::removeBook(qint32 id)
{
    const QString sql = "DELETE FROM " + c(Book::TABLE_NAME) + " WHERE "
            + eq(Table::KEY_ID, QString::number(id)) + ";";
    QSqlQuery query(mDB);
    query.prepare(sql);
    return remove(query);
}

bool DatabaseHandler::removeItem(qint32 id)
{
    const QString sql = "DELETE FROM " + c(Item::TABLE_NAME) + " WHERE "
            + eq(Table::KEY_ID, QString::number(id)) + ";";
    QSqlQuery query(mDB);
    query.prepare(sql);
    return remove(query);
}

bool DatabaseHandler::removeUser(qint32 id)
{
    const QString sql = "DELETE FROM " + c(User::TABLE_NAME) + " WHERE "
            + eq(Table::KEY_ID, QString::number(id)) + ";";
    QSqlQuery query(mDB);
    query.prepare(sql);
    return remove(query);
}

bool DatabaseHandler::add(QSqlQuery &query, Table &row)
{
    if(&row == NULL) {
        return false;
    }

    mDB.transaction();

    if(query.exec()) {
        mDB.commit();
        query.exec("SELECT LAST_INSERT_ID();");

        if(query.next()) {
            quint32 id = query.value(0).toInt();
            row.setId(id);
        } else {
            row.setId(Table::INVALID_ID);
        }

        return true;
    } else {
        log(query.lastQuery());
        row.setId(Table::INVALID_ID);
        mDB.rollback();
        return false;
    }
}

bool DatabaseHandler::update(QSqlQuery &query, const Table &row)
{
    if(&row == NULL) {
        return false;
    }

    mDB.transaction();

    if(query.exec()) {
        mDB.commit();
        return true;
    } else {
        log(query.lastQuery());
        mDB.rollback();
        return false;
    }
}

bool DatabaseHandler::tagsCommon(std::list<Tag> &tags, const QString &sql)
{
    QSqlQueryModel model;
    model.setQuery(sql);

    if(model.lastError().type() != QSqlError::NoError) {
        log(sql);
        return false;
    }

    const int count = model.rowCount();

    for(int i = 0; i < count; ++i) {
        auto record = model.record(i);
        Tag tag(record.value(Table::KEY_ID).toInt());
        tag.setLabel(record.value(Tag::KEY_LABEL).toString());
        qint32 parentId = record.isNull(Tag::KEY_PARENT_ID) ?
                    Table::INVALID_ID
                  : record.value(Tag::KEY_PARENT_ID).toInt();
        tag.setParentId(parentId);
        tags.push_back(tag);
    }

    return true;
}

bool DatabaseHandler::booksCommon(std::list<Book> &books, const QString &sql)
{
    QSqlQueryModel model;
    model.setQuery(sql);

    if(model.lastError().type() != QSqlError::NoError) {
        log(sql);
        return false;
    }

    const int count = model.rowCount();

    for(int i = 0; i < count; ++i) {
        auto record = model.record(i);
        Book book(record.value(Table::KEY_ID).toInt());
        book.setTitle(record.value(Book::KEY_TITLE).toString());
        book.setPages(record.value(Book::KEY_PAGES).toUInt());
        book.setYear(record.value(Book::KEY_YEAR).toUInt());
        book.setPublisher(record.value(Book::KEY_PUBLISHER).toString());
        book.setAuthors(record.value(Book::KEY_AUTHORS).toString());
        book.setCodes(record.value(Book::KEY_CODES).toString());
        qint32 tagId = record.isNull(Book::KEY_TAG_ID) ?
                    Table::INVALID_ID
                  : record.value(Book::KEY_TAG_ID).toInt();
        book.setTagId(tagId);
        books.push_back(book);
    }

    return true;
}

bool DatabaseHandler::remove(QSqlQuery &query)
{
    mDB.transaction();

    if(query.exec()) {
        mDB.commit();
        return true;
    } else {
        log(query.lastQuery());

        if(!mDB.rollback()) {
            qDebug() << "Cannot rollback last changes!";
        }
    }

    return false;
}

QString DatabaseHandler::c(const QString &columnName)
{
    return "`" + columnName + "`";
}

QString DatabaseHandler::eq(const QString &column, const QString &value)
{
    return c(column) + " = " + value;
}

void DatabaseHandler::log(const QString &sql)
{
    qDebug() << "Error in sql: " << sql;
}
