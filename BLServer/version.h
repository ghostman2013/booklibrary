#ifndef VERSION_H
#define VERSION_H
#define APP_NAME "Book Library Server"
#define VERSION_MAJOR 0U
#define VERSION_MINOR 1U
#define VERSION_BUILD 24U
#define VENDOR_NAME "ConteDevel DT"
#define VENDOR_SITE "http://www.contedevel.com"
#define AUTHOR_NAME "Denis Sologub"
#define AUTHOR_MAIL "contedevel2010@gmail.com"
#define YEAR 2015U
#endif // VERSION_H

