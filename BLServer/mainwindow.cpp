#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <utility>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow{parent},
    mServer{nullptr},
    mUI{new Ui::MainWindow},
    mLoginDialog{nullptr},
    mLogDialog{nullptr}
{
    mUI->setupUi(this);
    mUI->vlBookControls->setAlignment(Qt::AlignTop);
    mUI->vlUserControls->setAlignment(Qt::AlignTop);
    mDB = new DatabaseHandler();
    mTagId = Table::INVALID_ID;
}

MainWindow::~MainWindow()
{
    mDB->close();
    delete mUI;
}

void MainWindow::on_action_Connect_triggered()
{
    mLoginDialog = new LoginDialog(this);
    connect(mLoginDialog, SIGNAL(accept(const QString &, const QString &)),
            this, SLOT(login(const QString &, const QString &)));
    mLoginDialog->show();
}

void MainWindow::login(const QString &login, const QString &password)
{
    if(nullptr != mDB) {
        delete mDB;
    }

    QString database = mLoginDialog->database();
    QString host = mLoginDialog->host();
    quint16 port = mLoginDialog->port();
    mDB = new DatabaseHandler(database, port, host);

    if(mDB->connect(login, password)) {
        qDebug() << "Connected!";
        mUI->action_Create->setEnabled(true);
        mUI->action_Drop->setEnabled(true);
        mUI->pbNewBook->setEnabled(true);
        mUI->pbNewBranch->setEnabled(true);
        mUI->pbNewClass->setEnabled(true);
        mUI->pbRefresh->setEnabled(true);
        mUI->pbSignUp->setEnabled(true);
        mUI->pbUserRefresh->setEnabled(true);
        mUI->pbLog->setEnabled(true);
        mUI->pbLogClear->setEnabled(true);
        mUI->pbLogRefresh->setEnabled(true);
        mLoginDialog->close();
    } else {
        qDebug() << "Connection failed!";
    }

    buildTagTree();
}

void MainWindow::on_action_Run_triggered()
{
    if(nullptr != mServer) {
        delete mServer;
    }

    mServer = new Server(*mDB, this);
}

void MainWindow::on_action_Create_triggered()
{
    if(mDB->create()) {
        qDebug() << "Created!";
    } else {
        qDebug() << "Creation failed!";
    }
}

void MainWindow::on_action_Drop_triggered()
{
    if(mDB->drop()) {
        qDebug() << "Dropped!";
    } else {
        qDebug() << "Dropping failed!";
    }
}

void MainWindow::on_pbNewBranch_clicked()
{
    Tag tag;
    BranchDialog *dBranch = new BranchDialog(*mDB, tag, this);
    dBranch->show();
}

void MainWindow::on_pbNewClass_clicked()
{
    Book book;
    BookDialog *dBook = new BookDialog(*mDB, book, this);
    dBook->setModal(true);
    dBook->show();
}

void MainWindow::buildTagTree()
{
    mUI->twTree->clear();
    mUI->lwClasses->clear();
    mUI->lwBooks->clear();
    mItems.clear();
    std::list<Tag> tags;
    mDB->tags(tags);

    for(auto s = tags.begin(), e = tags.end(); s != e; ++s) {

        if(Table::INVALID_ID == s->parentId()) {
            QTreeWidgetItem *item = new QTreeWidgetItem(mUI->twTree);
            item->setText(0, s->label());
            buildTagNode(*item, s->id());
        }
    }
}

void MainWindow::buildTagNode(QTreeWidgetItem &parent, qint32 id)
{
    std::list<Tag> tags;
    mDB->tags(tags, id);

    for(auto s = tags.begin(), e = tags.end(); s != e; ++s) {

        if(id == s->parentId()) {
            QTreeWidgetItem *item = new QTreeWidgetItem(parent);
            item->setText(0, s->label());
            parent.addChild(item);
            buildTagNode(*item, s->id());
        }
    }
}

void MainWindow::buildBookList(qint32 tagId)
{
    mUI->lwClasses->clear();
    mUI->lwBooks->clear();
    mItems.clear();
    std::list<Book> books;
    mDB->books(books, tagId);

    for(auto s = books.begin(), e = books.end(); s != e; ++s) {
        mUI->lwClasses->addItem(s->title());
    }
}

void MainWindow::buildItemList(qint32 bookId)
{
    std::list<Item> items;
    mDB->items(items, bookId);

    for(auto s = items.begin(), e = items.end(); s != e; ++s) {
        mUI->lwBooks->addItem(s->inventoryCode());
        mItems.push_back(*s);
    }
}

void MainWindow::on_pbRefresh_clicked()
{
    buildTagTree();
}

void MainWindow::on_twTree_itemClicked(QTreeWidgetItem *item, int)
{
    const QString label = item->text(0);
    Tag tag;
    mDB->tag(tag, label);
    mTagId = tag.id();
    buildBookList(tag.id());
}

void MainWindow::on_lwClasses_itemSelectionChanged()
{
    mUI->lwBooks->clear();
    auto indeces = mUI->lwClasses->selectionModel()->selectedIndexes();
    std::list<Book> books;
    mDB->books(books, mTagId);

    foreach(QModelIndex index, indeces) {
        auto i = books.begin();
        std::advance(i, index.row());
        buildItemList(i->id());
    }
}

void MainWindow::on_pbNewBook_clicked()
{
    Item item;
    ItemDialog *dItem = new ItemDialog(*mDB, item);
    dItem->show();
}

void MainWindow::on_pbSignUp_clicked()
{
    User u;
    UserDialog *dUser = new UserDialog(*mDB, u, this);
    dUser->show();
}

void MainWindow::on_pbUserRefresh_clicked()
{
    while(mUI->twUsers->rowCount() > 0) {
        mUI->twUsers->removeRow(0);
    }

    std::list<User> users;
    mDB->users(users);
    int row = 0;

    for(auto s = users.begin(), e = users.end(); s != e; ++s) {
        mUI->twUsers->insertRow(row);
        mUI->twUsers->setItem(row, 0, new QTableWidgetItem(s->name()));
        mUI->twUsers->setItem(row, 1, new QTableWidgetItem(s->eMail()));
        QString role = (User::Role::Reader == s->role()) ?
                    "Reader" : "Administrator";
        mUI->twUsers->setItem(row, 2, new QTableWidgetItem(role));
        qint32 y = Utils::unpack(s->birthdate(), Utils::DateField::Year);
        qint32 m = Utils::unpack(s->birthdate(), Utils::DateField::Month);
        qint32 d = Utils::unpack(s->birthdate(), Utils::DateField::Day);
        QString date = QString("%1.%2.%3")
                .arg(d, 2, 10, QChar('0'))
                .arg(m, 2, 10, QChar('0'))
                .arg(y);
        mUI->twUsers->setItem(row++, 3, new QTableWidgetItem(date));
    }
}

void MainWindow::on_twTree_customContextMenuRequested(const QPoint &)
{
    if(mUI->twTree->selectedItems().size() == 1) {
        ContextMenu::show(1, &MainWindow::onTreeAction, this);
    }
}

void MainWindow::onTreeAction(QAction *action)
{
    auto items = mUI->twTree->selectedItems();
    auto label = items.at(0)->text(0);

    switch(action->data().toInt()) {
    case ContextMenu::Remove:
        if(!mDB->removeTag(label))
            qDebug() << "Removing of the selected tag failed!";

        buildTagTree();
        break;
    case ContextMenu::Edit:
        Tag tag;
        mDB->tag(tag, label);
        BranchDialog *dBranch = new BranchDialog(*mDB, tag, this);
        dBranch->show();
    }
}

void MainWindow::onClassesAction(QAction *action)
{
    auto indeces = mUI->lwClasses->selectionModel()->selectedIndexes();
    std::list<Book> books;
    mDB->books(books, mTagId);

    switch(action->data().toInt()) {
    case ContextMenu::Remove:
        foreach(QModelIndex index, indeces) {
            auto i = books.begin();
            std::advance(i, index.row());

            if(!mDB->removeBook(i->id())) {
                qDebug() << "Removing of book with ID "
                            + QString::number(i->id()) + " failed!";
            }
        }

        break;
    case ContextMenu::Edit:
        auto index = indeces.begin();
        auto b = books.begin();
        std::advance(b, index->row());
        BookDialog *dBook = new BookDialog(*mDB, *b, this);
        dBook->show();
    }
}

void MainWindow::onBooksAction(QAction *action)
{
    auto indeces = mUI->lwBooks->selectionModel()->selectedIndexes();

    switch(action->data().toInt()) {
    case ContextMenu::Remove:
        foreach(QModelIndex index, indeces) {
            auto i = mItems.begin();
            std::advance(i, index.row());

            if(!mDB->removeItem(i->id())) {
                qDebug() << "Removing of item with ID "
                            + QString::number(i->id()) + " failed!";
            }
        }

        break;
    case ContextMenu::Edit:
        auto index = indeces.begin();
        auto it = mItems.begin();
        std::advance(it, index->row());
        ItemDialog *dItem = new ItemDialog(*mDB, *it, this);
        dItem->show();
    }
}

void MainWindow::onUserAction(QAction *action)
{
    auto indeces = mUI->twUsers->selectionModel()->selectedIndexes();
    std::list<User> users;
    mDB->users(users);

    switch(action->data().toInt()) {
    case ContextMenu::Remove:
        foreach(QModelIndex index, indeces) {
            auto i = users.begin();
            std::advance(i, index.row());

            if(!mDB->removeUser(i->id())) {
                qDebug() << "Removing of user with ID "
                            + QString::number(i->id()) + " failed!";
            }
        }

        break;
    case ContextMenu::Edit:
        auto index = indeces.begin();
        auto u = users.begin();
        std::advance(u, index->row());

        UserDialog *dUser = new UserDialog(*mDB, *u, this);
        dUser->show();
    }
}

void MainWindow::on_lwClasses_customContextMenuRequested(const QPoint &)
{
    int count = mUI->lwClasses->selectedItems().size();
    ContextMenu::show(count, &MainWindow::onClassesAction, this);
}

void MainWindow::on_lwBooks_customContextMenuRequested(const QPoint &)
{
    int count = mUI->lwBooks->selectedItems().size();
    ContextMenu::show(count, &MainWindow::onBooksAction, this);
}

void MainWindow::on_twUsers_customContextMenuRequested(const QPoint &)
{
    int count = mUI->twUsers->selectionModel()->selectedRows().count();
    ContextMenu::show(count, &MainWindow::onUserAction, this);
}

void MainWindow::on_pbLog_clicked()
{
    if(mLogDialog != nullptr) {
        delete mLogDialog;
    }

    mLogDialog = new LogDialog(mDB, this);
    mLogDialog->show();
}

void MainWindow::on_pbLogRefresh_clicked()
{
    mUI->twLog->clear();
    std::list<Log> logs;
    mDB->logs(logs);
    int row = 0;

    QString actions[] = {"ISSUANCE", "RETURN", "REQUEST"};

    for(auto s = logs.begin(), e = logs.end(); s != e; ++s) {
        mUI->twLog->insertRow(row);
        User *user = new User();
        mDB->user(*user, s->userId());
        Item *item = new Item();
        mDB->item(*item, s->itemId());
        mUI->twLog->setItem(row, 0, new QTableWidgetItem(
                                QDateTime::fromMSecsSinceEpoch(s->timestamp())
                                .toString(Qt::SystemLocaleLongDate)));
        mUI->twLog->setItem(row, 1, new QTableWidgetItem(
                                std::move(user->name())));
        mUI->twLog->setItem(row, 2, new QTableWidgetItem(
                                std::move(user->eMail())));
        mUI->twLog->setItem(row, 3, new QTableWidgetItem(
                                std::move(item->inventoryCode())));
        mUI->twLog->setItem(row++, 4, new QTableWidgetItem(
                                actions[(int)s->action()]));
        delete item;
        delete user;
    }
}
