#ifndef BOOKDIALOG_H
#define BOOKDIALOG_H

#include <QDialog>
#include <QDebug>
#include <list>
#include <database/databasehandler.h>

namespace Ui {
class BookDialog;
}

class BookDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BookDialog(DatabaseHandler &db, Book &book, QWidget *parent = 0);
    ~BookDialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::BookDialog *mUI;
    DatabaseHandler mDB;
    Book mBook;
    std::list<Tag> mTags;
};

#endif // BOOKDIALOG_H
