#include "logdialog.h"
#include "ui_logdialog.h"

LogDialog::LogDialog(DatabaseHandler *db,
        QWidget *parent) :
    QDialog{parent},
    ui{new Ui::LogDialog},
    mDB{db}
{
    ui->setupUi(this);
    mDB->users(mUsers);

    for(auto s = mUsers.begin(), e = mUsers.end(); s != e; ++s)
        ui->cbUser->addItem(" <" + s->eMail() + ">" + s->name());

    mDB->tags(mTags);

    for(auto s = mTags.begin(), e = mTags.end(); s != e; ++s)
        ui->cbBranch->addItem(s->label());

    updateClasses(mTags.begin()->id());

    auto bOk = ui->buttonBox->button(QDialogButtonBox::Ok);
    bOk->setEnabled(false);
    //Tab focus order
    QWidget::setTabOrder(ui->cbUser, ui->cbBranch);
    QWidget::setTabOrder(ui->cbBranch, ui->cbClass);
    QWidget::setTabOrder(ui->cbClass, ui->cbBook);
    QWidget::setTabOrder(ui->cbBook, ui->buttonBox);
    //Run check
    check();
}

LogDialog::~LogDialog()
{
    delete ui;
}

void LogDialog::on_cbBranch_currentIndexChanged(int index)
{
    auto s = mTags.begin();
    std::advance(s, index);
    updateClasses(s->id());
    check();
}

void LogDialog::updateClasses(qint32 tagId)
{
    mBooks.clear();
    ui->cbClass->clear();
    mDB->books(mBooks, tagId);

    for(auto s = mBooks.begin(), e = mBooks.end(); s != e; ++s)
        ui->cbClass->addItem(s->title());
}

void LogDialog::updateBooks(qint32 bookId)
{
    mItems.clear();
    ui->cbBook->clear();
    mDB->items(mItems, bookId);

    for(auto s = mItems.begin(), e = mItems.end(); s != e; ++s)
        ui->cbBook->addItem(s->inventoryCode());
}

void LogDialog::check()
{
    bool result = ui->cbUser->currentIndex() != -1
            && ui->cbBranch->currentIndex() != -1
            && ui->cbClass->currentIndex() != -1
            && ui->cbBook->currentIndex() != -1;
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(result);
}

void LogDialog::on_cbClass_currentIndexChanged(int index)
{
    auto s = mBooks.begin();
    std::advance(s, index);
    updateBooks(s->id());
    check();
}

void LogDialog::on_buttonBox_accepted()
{
    auto log = new Log();
    //User ID
    auto sU = mUsers.begin();
    std::advance(sU, ui->cbUser->currentIndex());
    log->setUserId(sU->id());
    //Item ID
    auto sI = mItems.begin();
    std::advance(sI, ui->cbBook->currentIndex());
    log->setItemId(sI->id());
    //Action
    log->setAction(static_cast<Log::Action>(ui->cbAction->currentIndex()));
    //Save in database
    if(!mDB->add(*log)) qDebug() << "Log insertion failed!";
    delete log;
    close();
}
