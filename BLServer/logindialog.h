#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <database/databasehandler.h>

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();
    QString database() const;
    QString host() const;
    int port() const;
signals:
    void accept(const QString &login, const QString &password);
private slots:
    void on_buttonBox_accepted();

private:
    Ui::LoginDialog *ui;
};

#endif // LOGINDIALOG_H
