#ifndef TEMPLATE_H
#define TEMPLATE_H
#include <type_traits>
#define TEMPLATE_SUBCLASS(parent, child) typename child,\
    typename std::enable_if<std::is_base_of<parent, child>::value>\
    ::type* = nullptr
#endif // TEMPLATE_H

