#include "userdialog.h"
#include "ui_userdialog.h"

UserDialog::UserDialog(DatabaseHandler &db, User &user, QWidget *parent):
    QDialog{parent},
    mUI{new Ui::UserDialog},
    mDB{db},
    mUser{user}
{
    mUI->setupUi(this);
    mUI->deBirthdate->setDate(QDate::currentDate());
    //Tab focus order
    QWidget::setTabOrder(mUI->leMail, mUI->lePassword);
    QWidget::setTabOrder(mUI->lePassword, mUI->leName);
    QWidget::setTabOrder(mUI->leName, mUI->deBirthdate);
    QWidget::setTabOrder(mUI->deBirthdate, mUI->cbRole);
    QWidget::setTabOrder(mUI->cbRole, mUI->buttonBox);

    if(user.id() != Table::INVALID_ID) {
        mUI->leMail->setText(user.eMail());
        mUI->leName->setText(user.name());
        mUI->deBirthdate->setDate(Utils::unpack(user.birthdate()));
        mUI->cbRole->setCurrentIndex((int)user.role());
    }
    //Validators
    QRegExp rxEMail("\\b[^|@]+@[^|@\\.]+\\.[A-Za-z]{2,4}\\b");
    QRegExp rxPass("\\b[^|]*\\b");
    mUI->leMail->setValidator(new QRegExpValidator(rxEMail, this));
    mUI->lePassword->setValidator(new QRegExpValidator(rxPass, this));
}

UserDialog::~UserDialog()
{
    delete mUI;
}

void UserDialog::on_buttonBox_accepted()
{
    auto eMail = mUI->leMail->text();
    auto password = Utils::md5(mUI->lePassword->text());
    auto name = mUI->leName->text();
    auto birthdate = Utils::pack(mUI->deBirthdate->date());
    mUser.setEMail(eMail);
    mUser.setPassword(password);
    mUser.setName(name);
    mUser.setBirthdate(birthdate);
    mUser.setRole(static_cast<User::Role>(mUI->cbRole->currentIndex()));

    if(Table::INVALID_ID == mUser.id() && !mDB.add(mUser))
        qDebug() << "User insertion failed!";
    else if(!mDB.update(mUser))
        qDebug() << "User update failed!";
}
