#include "branchdialog.h"
#include "ui_branchdialog.h"

BranchDialog::BranchDialog(DatabaseHandler &db, Tag &tag, QWidget *parent) :
    QDialog{parent},
    mUI{new Ui::BranchDialog},
    mDB{db},
    mTag{tag}
{
    mUI->setupUi(this);
    mUI->cbParent->addItem("None");

    if(mDB.tags(mTags))
        for(auto i = mTags.begin(), e = mTags.end(); i != e;)
            if(i->id() == tag.id())
                i = mTags.erase(i);
            else {
                mUI->cbParent->addItem(i->label());
                ++i;
            }

    if(Table::INVALID_ID != tag.id()) {
        mUI->leLabel->setText(tag.label());

        if(Table::INVALID_ID == tag.parentId())
            mUI->cbParent->setCurrentIndex(0);
        else
            for(auto i = mTags.begin(), e = mTags.end(); i != e; ++i)
                if(i->id() == tag.parentId()) {
                    int position = std::distance(mTags.begin(), i) + 1;
                    mUI->cbParent->setCurrentIndex(position);
                }
    }
    //Tab focus order
    QWidget::setTabOrder(mUI->cbParent, mUI->leLabel);
    QWidget::setTabOrder(mUI->leLabel, mUI->buttonBox);
}

BranchDialog::~BranchDialog()
{
    delete mUI;
}

void BranchDialog::on_buttonBox_accepted()
{
    auto label = mUI->leLabel->text();
    mTag.setLabel(label);
    int index = mUI->cbParent->currentIndex();

    if(index == 0)
        mTag.setParentId(Table::INVALID_ID);
    else {
        auto i = mTags.begin();
        std::advance(i, index - 1);
        mTag.setParentId(i->id());
    }

    if(Table::INVALID_ID == mTag.id() && !mDB.add(mTag))
        qDebug() << "Tag writing failed!";
    else if(!mDB.update(mTag))
        qDebug() << "Tag update failed!";
}
