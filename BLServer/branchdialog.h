#ifndef BRANCHDIALOG_H
#define BRANCHDIALOG_H

#include <QDialog>
#include <QDebug>
#include "database/databasehandler.h"

namespace Ui {
class BranchDialog;
}

class BranchDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BranchDialog(DatabaseHandler &db, Tag &tag, QWidget *parent = 0);
    ~BranchDialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::BranchDialog *mUI;
    DatabaseHandler mDB;
    Tag mTag;
    std::list<Tag> mTags;
};

#endif // BRANCHDIALOG_H
