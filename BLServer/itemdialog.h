#ifndef ITEMDIALOG_H
#define ITEMDIALOG_H

#include <QDialog>
#include <QDebug>
#include <database/databasehandler.h>

namespace Ui {
class ItemDialog;
}

class ItemDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ItemDialog(DatabaseHandler &db,
                        Item &item,
                        QWidget *parent = 0);
    ~ItemDialog();

private slots:
    void on_ItemDialog_accepted();

private:
    Ui::ItemDialog *mUI;
    DatabaseHandler mDB;
    Item mItem;
    std::list<Book> mBooks;
};

#endif // ITEMDIALOG_H
