#include "client.h"

Client::Client(DatabaseHandler &db, QTcpSocket *socket, QObject *parent) :
    QObject{parent},
    mSocket{socket},
    mBlockSize{0},
    mDB{db}
{
    connect(mSocket, SIGNAL(disconnected()), this, SLOT(onDisconnected()));
    connect(mSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
}

void Client::onDisconnected() {
    qDebug() << "On disconnected!";
    emit disconnected(this);
}

void Client::onReadyRead() {
    qDebug() << "On ready read!";
    QDataStream in(mSocket);
    in.setVersion(QDataStream::Qt_4_0);

    for (;;) {

        if(!mBlockSize) {
            if (mSocket->bytesAvailable() < (quint32)sizeof(quint32)) break;
            in >> mBlockSize;
        }

        if(mSocket->bytesAvailable() < mBlockSize) break;

        qDebug() << "Package was recieved!";
        qDebug() << "Block size: " << mBlockSize;

        if(Table::INVALID_ID == mUser.id()) {
            QString email;
            QString password;
            in >> email;
            in >> password;
            qDebug() << "Recieved account data: " << email << " " << password;

            if(mDB.user(mUser, email, Utils::md5(password))
                    && Table::INVALID_ID != mUser.id()) {
                qDebug() << "Authentication successed!";
                std::list<Tag> tags;

                if(mDB.tags(tags)) {
                    QList<QString> items;

                    for(auto s = tags.begin(), e = tags.end(); s != e; ++s) {
                        items.append(QString::number(s->id()));
                        items.append(s->label());
                        items.append(QString::number(s->parentId()));
                    }

                    sendToClient("Tags", items);
                }
            } else {
                mSocket->disconnectFromHost();
                qDebug() << "Authentication failed!";
                QList<QString> args;
                args.append("Incorrect login or password!");
                sendToClient("Error", args);
            }
        }

        mBlockSize = 0;
    }
}

void Client::sendToClient(const QString &tag, QList<QString> &items)
{
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);
    out << quint32(0) << tag;
    out << quint32(items.count());

    for(int i = 0; i < items.count(); ++i) {
        out << items.at(i);
    }

    out.device()->seek(0);
    quint32 blockSize = arrBlock.size() - sizeof(quint32);
    qDebug() << "Sent " << blockSize << " byte(s)";
    out << blockSize;
    mSocket->write(arrBlock);
}

