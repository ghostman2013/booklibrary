#include "server.h"
#include "QThread"

Server::Server(DatabaseHandler &db, QObject *parent) :
    QObject{parent},
    mDB{db}
{
    mServer = new QTcpServer(this);
    connect(mServer, SIGNAL(newConnection()), this, SLOT(newConnection()));

    if(mServer->listen(QHostAddress::AnyIPv4, 9999)) {
        qDebug() << "Server was launched!";
        mState = ServerState::Working;
    } else {
        qDebug() << "Server couldn't start!";
        mState = ServerState::Stopped;
    }
}

void Server::newConnection()
{
    qDebug() << "New connection!";
    auto socket = mServer->nextPendingConnection();
    auto client = QSharedPointer<Client>(new Client(mDB, socket, this),
                                         &QObject::deleteLater);
    connect(client.data(), SIGNAL(disconnected(Client*)),
            this, SLOT(removeClient(Client*)));
    mClients.append(client);
}

void Server::removeClient(Client *client)
{
    for(int i = 0; i < mClients.count(); ++i) {
        auto p = mClients.at(i);

        if(p.data() == client) {
            p.clear();
            mClients.removeAt(i);
        }
    }
}

