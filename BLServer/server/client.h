#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <list>
#include <QList>
#include <QDebug>
#include "database/databasehandler.h"
#include "database/utils.h"

class Client : public QObject
{
    Q_OBJECT
public:
    Client(DatabaseHandler &db, QTcpSocket *socket, QObject *parent = 0);
signals:
    void disconnected(Client *client);
public slots:
    void onDisconnected();
    void onReadyRead();
private:
    void sendToClient(const QString &tag, QList<QString> &items);
    QTcpSocket *mSocket;
    quint32 mBlockSize;
    User mUser;
    DatabaseHandler mDB;
};

#endif // CLIENT_H
