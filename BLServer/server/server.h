#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QDebug>
#include <QList>
#include <QSharedPointer>
#include "client.h"
#include "database/databasehandler.h"

class Server : public QObject
{
    Q_OBJECT
public:
    enum class ServerState {
        Working = 0,
        Stopped = 1
    };

    explicit Server(DatabaseHandler &db, QObject *parent = 0);
public slots:
    void newConnection();
    void removeClient(Client *client);
private:
    QTcpServer *mServer;
    QList<QSharedPointer<Client>> mClients;
    ServerState mState;
    DatabaseHandler mDB;
};

#endif // SERVER_H
