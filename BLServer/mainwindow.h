#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include "server/server.h"
#include "database/databasehandler.h"
#include "database/utils.h"
#include "logindialog.h"
#include "branchdialog.h"
#include "bookdialog.h"
#include "itemdialog.h"
#include "userdialog.h"
#include "logdialog.h"
#include "menu/contextmenu.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    typedef void (MainWindow::*MenuListener)(QAction *);

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_action_Connect_triggered();
    void on_action_Run_triggered();
    void on_action_Create_triggered();
    void on_action_Drop_triggered();
    void on_pbNewBranch_clicked();
    void on_pbNewClass_clicked();
    void on_pbRefresh_clicked();
    void on_twTree_itemClicked(QTreeWidgetItem *item, int);
    void on_lwClasses_itemSelectionChanged();
    void on_pbNewBook_clicked();
    void on_pbSignUp_clicked();
    void on_pbUserRefresh_clicked();
    void on_twTree_customContextMenuRequested(const QPoint &);
    void on_lwClasses_customContextMenuRequested(const QPoint &);
    void on_lwBooks_customContextMenuRequested(const QPoint &);
    void on_twUsers_customContextMenuRequested(const QPoint &);
    void onTreeAction(QAction *action);
    void onClassesAction(QAction *action);
    void onBooksAction(QAction *action);
    void onUserAction(QAction *action);
    void login(const QString &login, const QString &password);
    void on_pbLog_clicked();
    void on_pbLogRefresh_clicked();

private:
    void buildTagTree();
    void buildTagNode(QTreeWidgetItem &parent, qint32 id);
    void buildBookList(qint32 tagId);
    void buildItemList(qint32 bookId);
    DatabaseHandler * mDB;
    Server *mServer;
    Ui::MainWindow *mUI;
    qint32 mTagId;
    LoginDialog *mLoginDialog;
    LogDialog *mLogDialog;
    std::list<Item> mItems;
};

#endif // MAINWINDOW_H
