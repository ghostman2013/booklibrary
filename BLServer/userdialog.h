#ifndef USERDIALOG_H
#define USERDIALOG_H

#include <QDialog>
#include <database/databasehandler.h>
#include <database/utils.h>

namespace Ui {
class UserDialog;
}

class UserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UserDialog(DatabaseHandler &db, User &user, QWidget *parent = 0);
    ~UserDialog();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::UserDialog *mUI;
    DatabaseHandler mDB;
    User mUser;
};

#endif // USERDIALOG_H
